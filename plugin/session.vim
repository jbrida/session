com! -nargs=? -complete=custom,session#complete_suggestions MakeSession call session#make_session(<f-args>)
com! -nargs=1 -complete=custom,session#complete_suggestions LoadSession call session#load_session(<f-args>)
