if exists('g:loaded_session')
	finish
endif

let g:loaded_session = 1
let $VIMHOME = $HOME . '/.vim'
let $VIMSESSIONS = $VIMHOME . '/sessions'

fun! session#make_session(...)
	if a:0 == 1
		let g:session = a:1
	endif

	if exists("g:session")
		exe 'mks! ' . $VIMSESSIONS . '/' . g:session . '.vim'
	endif
endfun

fun! session#load_session(session)
	let g:session = a:session
	exe 'so ' . $VIMSESSIONS . '/' . g:session . '.vim'
endfun

fun! session#complete_suggestions(arg_lead, cmd_line, cursor_pos)
	return system('ls -1 ' . $VIMSESSIONS . ' | cut -d "." -f 1')
endfun
